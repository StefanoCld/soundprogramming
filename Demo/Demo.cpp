#include "../FModWrapperProject/FModWrapperProject.h"

#include <iostream>

const static int SOUNDS_NUM = 3;

//Sounds names
const static char* soundPath[SOUNDS_NUM] =
{ "darkAmbient.wav", "arrow.wav" , "sword.wav" };

void PrintMenu(FModWrapper* const f);
void ClearScreen();
void LoadStaticSound(FModWrapper* const f);
void LoadStreamSound(FModWrapper* const f);
void Play(FModWrapper* const f);
void Pause(FModWrapper* const f);
void Stop(FModWrapper* const f);
void SetLoop(FModWrapper* const f);
void SetVolume(FModWrapper* const f);
void SetPan(FModWrapper* const f);
void Exit();

int main()
{
    int choice = 0;
    FModWrapper* f = new FModWrapper();
    f->Init();

    do
    {
        PrintMenu(f);
        std::cin >> choice;
        std::cin.ignore(1);

        switch (choice) 
        {
        case 1:
            LoadStaticSound(f);
            break;

        case 2:
            LoadStreamSound(f);
            break;

        case 3:
            Play(f);
            break;

        case 4:
            Pause(f);
            break;

        case 5:
            Stop(f);
            break;

        case 6:
            SetLoop(f);
            break;

        case 7:
            SetPan(f);
            break;

        case 8:
            SetVolume(f);
            break;

        case 9:
            Exit();
            break;

        default:
            std::cout << "Invalid choice!" << std::endl;
        }
    } 
    while (choice != 9);
}

void PrintMenu(FModWrapper* const f)
{
    std::cout << "\n *** FMOD Wrapper Demo *** \n" << std::endl;
    std::cout << "Channels indexes: 0 -> " << f->GetChannelsNumber() - 1 << std::endl;
    std::cout << "{1} Load sound (static)" << std::endl;
    std::cout << "{2} Load sound (streaming)" << std::endl;
    std::cout << "{3} Play" << std::endl;
    std::cout << "{4} Pause" << std::endl;
    std::cout << "{5} Stop" << std::endl;
    std::cout << "{6} Set Loop" << std::endl;
    std::cout << "{7} Set Pan" << std::endl;
    std::cout << "{8} Set Volume" << std::endl;
    std::cout << "{9} Exit" << std::endl;
}
void ClearScreen()
{
    system("cls");
}

void LoadStaticSound(FModWrapper* const f) 
{
    int sound;
    int channel;

    ClearScreen();

    std::cout << "Load sound (static) - Select the sound: " << std::endl;
    std::cout << "[0] Ambient" << std::endl;
    std::cout << "[1] Arrow" << std::endl;
    std::cout << "[2] Sword" << std::endl;

    std::cin >> sound;
    std::cin.ignore(1);

    std::cout << "Load sound (static) - Select the channel: " << std::endl;
    std::cin >> channel;
    std::cin.ignore(1);

    bool result;

    if (sound >= 0 && sound < SOUNDS_NUM)
    {
        result = f->LoadStaticSound(soundPath[sound], channel);

        if (result == true) 
        {
            std::cout << "Sound loaded succesfully!" << std::endl;
        }
        else 
        {
            std::cout << "Error!" << std::endl;
        }

    }
    else 
    {
        std::cout << "Wrong sound index";
    }
    
}
void LoadStreamSound(FModWrapper* const f)
{
    int sound;
    int channel;

    ClearScreen();

    std::cout << "Load sound (streaming) - Select the sound: " << std::endl;
    std::cout << "[0] Ambient" << std::endl;
    std::cout << "[1] Laser" << std::endl;
    std::cout << "[2] Sword" << std::endl;

    std::cin >> sound;
    std::cin.ignore(1);

    std::cout << "Load sound (streaming) - Select the channel: " << std::endl;
    std::cin >> channel;
    std::cin.ignore(1);

    bool result;

    if (sound >= 0 && sound < SOUNDS_NUM)
    {
        result = f->LoadStreamingSound(soundPath[sound], channel);

        if (result == true)
        {
            std::cout << "Sound loaded succesfully!" << std::endl;
        }
        else
        {
            std::cout << "Error!" << std::endl;
        }

    }
    else
    {
        std::cout << "Wrong sound index";
    }

}
void Play(FModWrapper* const f)
{
    int channel;

    ClearScreen();

    std::cout << "Play sound - Select the channel: " << std::endl;
    std::cin >> channel;
    std::cin.ignore(1);

    bool result;

    result = f->PlaySound(channel);

    if (result == true)
    {
        std::cout << "Sound played succesfully!" << std::endl;
    }
    else
    {
        std::cout << "Error!" << std::endl;
    }
}

void Pause(FModWrapper* const f)
{
    int channel;

    ClearScreen();

    std::cout << "Pause sound - Select the channel: " << std::endl;
    std::cin >> channel;
    std::cin.ignore(1);

    bool result;

    result = f->PauseSound(channel);

    if (result == true)
    {
        std::cout << "Sound paused succesfully!" << std::endl;
    }
    else
    {
        std::cout << "Error!" << std::endl;
    }
}

void Stop(FModWrapper* const f) 
{
    int channel;

    ClearScreen();

    std::cout << "Stop sound - Select the channel: " << std::endl;
    std::cin >> channel;
    std::cin.ignore(1);

    bool result;

    result = f->StopSound(channel);

    if (result == true)
    {
        std::cout << "Sound stopped succesfully!" << std::endl;
    }
    else
    {
        std::cout << "Error!" << std::endl;
    }
}

void SetLoop(FModWrapper* const f) 
{
    int channel;
    int willLoop;

    ClearScreen();

    std::cout << "Loop sound - Select the channel: " << std::endl;
    std::cin >> channel;
    std::cin.ignore(1); 
    std::cout << "Do you want to enable looping? " << std::endl;
    std::cout << "[0] Y " << std::endl;
    std::cout << "[1] N " << std::endl;
    std::cin >> willLoop;
    std::cin.ignore(1);

    bool result;

    result = f->LoopSound(channel, willLoop == 0 ? true : false);

    if (result == true)
    {
        std::cout << "Loop set succesfully!" << std::endl;
    }
    else
    {
        std::cout << "Error!" << std::endl;
    }
}

void SetVolume(FModWrapper* const f) 
{
    int channel;
    int volume;

    bool result;

    ClearScreen();

    std::cout << "Set sound volume - Select the channel: " << std::endl;
    std::cin >> channel;
    std::cin.ignore(1);
    std::cout << "Set sound volume - Value ( 0 : 100 )" << std::endl;
    std::cin >> volume;
    std::cin.ignore(1);

    if (volume <= 0)
        volume = 0;

    if (volume > 100)
        volume = 100;

    float fVolume = ((float)volume) / 100;

    result = f->SetVolume(channel, fVolume);

    if (result == true)
    {
        std::cout << "Volume set succesfully!" << std::endl;
    }
    else
    {
        std::cout << "Error!" << std::endl;
    }
}

void SetPan(FModWrapper* const f) 
{
    int channel;
    int pan;

    bool result;

    ClearScreen();

    std::cout << "Set sound pan - Select the channel: " << std::endl;
    std::cin >> channel;
    std::cin.ignore(1);
    std::cout << "Set sound pan - Value ( -100 : 100 )" << std::endl;
    std::cin >> pan;
    std::cin.ignore(1);

    if (pan <= -100)
        pan = -100;

    if (pan > 100)
        pan = 100;

    float fPan = ((float)pan) / 100;
    std::cout << fPan << std::endl;

    result = f->SetPan(channel, fPan);

    if (result == true)
    {
        std::cout << "Pan set succesfully!" << std::endl;
    }
    else
    {
        std::cout << "Error!" << std::endl;
    }
}

void Exit()
{
    std::cout << "Goodbye!" << std::endl;
}