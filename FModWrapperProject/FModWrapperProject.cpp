#include "pch.h"
#include "framework.h"
#include "FModWrapperProject.h"

#include <iostream>
#include <string>
#include <vector>

#include <fmod.hpp>
#include <fmod_errors.h>

//Private Methods
ChannelState FModWrapper::VerifyChannel(FMOD::Channel* const channel) 
{
	bool isPlaying = false;

	FMOD_RESULT result = channel->isPlaying(&isPlaying);

	if (result != FMOD_RESULT::FMOD_OK 
		&& result != FMOD_RESULT::FMOD_ERR_INVALID_HANDLE 
		&& result != FMOD_ERR_CHANNEL_STOLEN)
	{
		return ChannelState::STOPPED;
	}

	if (isPlaying) 
	{
		bool isPaused = false;

		result = channel->getPaused(&isPaused);

		if (result != FMOD_RESULT::FMOD_OK)
		{
			return ChannelState::STOPPED;
		}

		if (isPaused)
		{
			return ChannelState::PAUSED;
		}

		return ChannelState::PLAYING;
	}

	return ChannelState::STOPPED;
}

//Public Methods
FModWrapper::FModWrapper() :
	sounds(CHANNELS_NUMBER, nullptr),
	channels(CHANNELS_NUMBER, nullptr)
{
	std::cout << "FModWrapper created!" << std::endl;
}

void FModWrapper::Init()
{
	std::cout << "FModWrapper is being initialized.. " << std::endl;

	FMOD_RESULT result = FMOD::System_Create(&system);

	if (result != FMOD_RESULT::FMOD_OK)
	{
		std::cout << "Cannot create system!" << std::endl;
		exit(-1);
	}

	result = system->init(CHANNELS_NUMBER, FMOD_INIT_NORMAL, 0);

	if (result != FMOD_RESULT::FMOD_OK)
	{
		std::cout << "Cannot init channels!" << std::endl;
		exit(-1);
	}
}

bool FModWrapper::LoadStaticSound(const char* filepath, int channelIndex)
{
	if (channelIndex < 0 || channelIndex > CHANNELS_NUMBER - 1)
		return false;

	FMOD_RESULT result = system->createSound(filepath, FMOD_CREATESAMPLE,
		nullptr, &(sounds.at(channelIndex)));

	if (result != FMOD_RESULT::FMOD_OK)
	{
		std::cout << "Sound not loaded";
		return false;
	}

	return true;
}

bool FModWrapper::LoadStreamingSound(const char* filepath, int channelIndex)
{
	if (channelIndex < 0 || channelIndex > CHANNELS_NUMBER - 1)
		return false;

	FMOD_RESULT result = system->createSound(filepath, FMOD_CREATESTREAM,
		nullptr, &(sounds.at(channelIndex)));

	if (result != FMOD_RESULT::FMOD_OK)
	{
		std::cout << "Sound not loaded";
		return false;
	}

	return true;
}

bool FModWrapper::PlaySound(int channelIndex)
{
	if (channelIndex < 0 || channelIndex > CHANNELS_NUMBER - 1)
		return false;

	ChannelState state = VerifyChannel(channels.at(channelIndex));

	if (state == ChannelState::PAUSED) 
	{
		FMOD_RESULT result = channels.at(channelIndex)->setPaused(false);

		if (result != FMOD_RESULT::FMOD_OK)
		{
			return false;
		}
	}

	else if (state == ChannelState::STOPPED && sounds.at(channelIndex))
	{
		FMOD_RESULT result = system->playSound(sounds.at(channelIndex),
			0, false, &channels.at(channelIndex));

		if (result != FMOD_RESULT::FMOD_OK)
		{
			return false;
		}
	}
	
	return true;
}

bool FModWrapper::PauseSound(int channelIndex)
{
	if (channelIndex < 0 || channelIndex > CHANNELS_NUMBER - 1)
		return false;

	ChannelState state = VerifyChannel(channels.at(channelIndex));

	if (state == ChannelState::PLAYING) 
	{
		FMOD_RESULT result = channels.at(channelIndex)->setPaused(true);

		if (result != FMOD_RESULT::FMOD_OK)
		{
			return false;
		}
	}

	return true;
}

bool FModWrapper::StopSound(int channelIndex)
{
	if (channelIndex < 0 || channelIndex > CHANNELS_NUMBER - 1)
		return false;

	ChannelState state = VerifyChannel(channels.at(channelIndex));

	if (state != ChannelState::STOPPED)
	{
		FMOD_RESULT result = channels.at(channelIndex)->stop();
		if (result != FMOD_RESULT::FMOD_OK)
		{
			return false;
		}
	}

	return true;
}

bool FModWrapper::LoopSound(int channelIndex, bool bLoop)
{
	if (channelIndex < 0 || channelIndex > CHANNELS_NUMBER - 1)
		return false;
		
	bool isPlaying = VerifyChannel(channels.at(channelIndex)) != ChannelState::STOPPED;
		
	if (isPlaying) {
		StopSound(channelIndex);
	}

	FMOD_RESULT result = sounds.at(channelIndex)->setMode(bLoop ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF);

	if (result != FMOD_RESULT::FMOD_OK)
	{
		return false;
	}

	return true;
}

bool FModWrapper::SetVolume(int channelIndex, float volume)
{
	if (channelIndex < 0 || channelIndex > CHANNELS_NUMBER - 1)
		return false;

	//Clamp value
	if (volume > 1)
		volume = 1;
	if (volume <= 0)
		volume = 0;

	FMOD_RESULT result = channels.at(channelIndex)->setVolume(volume);

	if (result != FMOD_RESULT::FMOD_OK 
		&& result != FMOD_RESULT::FMOD_ERR_INVALID_HANDLE)
	{
		return false;
	}

	return true;
}

bool FModWrapper::SetPan(int channelIndex, float pan)
{
	if (channelIndex < 0 || channelIndex > CHANNELS_NUMBER - 1)
		return false;

	//Clamp value
	if (pan > 1)
		pan = 1;
	if (pan < -1)
		pan = -1;

	FMOD_RESULT result = channels.at(channelIndex)->setPan(pan);

	if (result != FMOD_RESULT::FMOD_OK
		&& result != FMOD_RESULT::FMOD_ERR_INVALID_HANDLE)
	{
		return false;
	}

	return true;
}

FModWrapper::~FModWrapper()
{
	for (FMOD::Channel* channel : channels)
		if(channel)
			channel->stop();

	for (FMOD::Sound* sound : sounds)
		if(sound)
			sound->release();
	
	if (system)
		system->release();
}

size_t FModWrapper::GetChannelsNumber() const
{
	return channels.size();
}