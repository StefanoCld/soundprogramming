#pragma once

#include <vector>

namespace FMOD {
	class System;
	class Channel;
	class Sound;
}

enum ChannelState {
	PLAYING,
	STOPPED,
	PAUSED
};

class FModWrapper
{
	const static int CHANNELS_NUMBER = 5;

	FMOD::System* system = 0;
	std::vector<FMOD::Sound*> sounds;
	std::vector<FMOD::Channel*> channels;

private:
	ChannelState VerifyChannel(FMOD::Channel* const channel);

public:
	FModWrapper();
	~FModWrapper();

	void Init();
	bool LoadStaticSound	(const char* filepath, int channelIndex);
	bool LoadStreamingSound	(const char* filepath, int channelIndex);
	bool PlaySound			(int channelIndex);
	bool PauseSound			(int channelIndex);
	bool StopSound			(int channelIndex);
	bool LoopSound			(int channelIndex, bool bLoop);
	bool SetVolume			(int channelIndex, float volume);
	bool SetPan				(int channelIndex, float pan);
	size_t GetChannelsNumber()const;
};